import { Component, ViewChild } from '@angular/core';
import { WeatherDetailsComponent } from '../weather/weather-details.component';

const CITIES_MAP: any[] = [
	{city: "bucharest", country: "ro"},
	{city: "vienna", country: "at"},
	{city: "zurich", country: "ch"}
];

@Component({
	moduleId: module.id,
	selector: 'dashboard',
	templateUrl: 'dashboard.component.html',
	styleUrls: ['dashboard.component.css']
})

export class DashboardComponent {
	@ViewChild(WeatherDetailsComponent) weatherDetails: WeatherDetailsComponent;

	selectedCityMap: any;
	citiesMap: any[];

	constructor() {
		this.citiesMap = CITIES_MAP;
		//auto-select the first city in the list
		this.selectedCityMap = this.citiesMap[0];
	}

	checkWeather(cityMap: any): void {
		this.selectedCityMap = cityMap;
		this.weatherDetails.checkWeather(this.selectedCityMap);
	}
}