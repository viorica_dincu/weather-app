import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class WeatherService {
	private weatherUrl: string;
	private baseUrl: string = "https://query.yahooapis.com/v1/public/yql?q=";

	constructor(private http: Http) { }

	getWeatherForCity(city: string, country: string): Promise<any> {
		this.weatherUrl = this.createWeatherUrl(city, country);
		return this.http.get(this.weatherUrl)
	           .toPromise()
	           .then((response: any) => {
					if (!response || !response._body) {
						return Promise.reject("The response doesn't contain any valid data");
					}
					var resp: any = JSON.parse(response._body);
					var results: any = resp.query.results;
					var respObj: any = {};

					if (!results || !results.channel) {
					return Promise.reject("The response doesn't contain any valid data");
					}

					respObj.condition = results.channel.item.condition;
					respObj.forecast = results.channel.item.forecast;

					return Promise.resolve(respObj);
	           		}
	           	)
	           .catch(this.handleError);
	}

	private createWeatherUrl(city: string, country: string): string {
		var encodeUrl: string;

		encodeUrl = encodeURIComponent('select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="' + city +  ', ' + country + '")') + '&format=json&env=store' + encodeURIComponent('://datatables.org/alltableswithkeys');

		return this.baseUrl + encodeUrl;
	}

	private handleError(error: any): Promise<any> {
		console.error('An error occurred', error);
		return Promise.reject(error.message || error);
	}
}