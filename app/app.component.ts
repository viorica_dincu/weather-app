import { Component } from '@angular/core';

@Component({
	selector: 'weather-app',
	template: `
		<div class="wrapper">
			<h1 >{{title}}</h1>
			<router-outlet></router-outlet>
		</div>
	`
})

export class AppComponent {
	title = 'Weather forecast and Conditions';
}