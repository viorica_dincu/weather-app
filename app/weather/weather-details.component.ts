import { Component, Input } from '@angular/core';
import { OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';

@Component ({
	moduleId: module.id,
	selector: 'weather-details',
	templateUrl: 'weather-details.component.html',
	styleUrls: ['weather-details.component.css']
})

export class WeatherDetailsComponent {
	@Input() cityMap: any;
    cityWeather: any;

    img1: string;
    img2: string;
    img3: string;

	constructor(private weatherService: WeatherService){
		this.img1 = "/app/img/default.png";
		this.img2 = "/app/img/default.png";
		this.img3 = "/app/img/default.png";
	}

	ngOnInit() {
		this.checkWeather(this.cityMap);
	}

	checkWeather(cityMap: any): void {
		this.weatherService.getWeatherForCity(cityMap.city, cityMap.country).then(
			weatherDetails => {
				this.cityWeather = weatherDetails;

				var forecast = this.cityWeather.forecast;
				this.img1 = this.weatherImgMap(forecast[0].text);
				this.img2 = this.weatherImgMap(forecast[1].text);
				this.img3 = this.weatherImgMap(forecast[2].text);
			}
		);
	}

	weatherImgMap(description: string): string {
		description = description.toLowerCase();
		if(description.indexOf("cloud") != -1) {
			return "app/img/cloud.png";
		}
		if(description.indexOf("snow") != -1) {
			return "app/img/snow.png";
		}
		if(description.indexOf("wind") != -1) {
			return "app/img/wind.png";
		}
		if(description.indexOf("sun") != -1) {
			return "app/img/sun.png";
		}
		if(description.indexOf("shower") != -1 || description.indexOf("rain") != -1) {
			return "app/img/rain.png";
		}
		return "app/img/default.png";
	}
}